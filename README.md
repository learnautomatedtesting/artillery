setup the policy for customers
A AWS key and secret are needed with sufficient permissions and a gitlab account.
Store your variables of AWS in the variables section

the pipeline creates a cdk synth, if the synth is ok, you can press on deploy and it will create the iam role
The first time you work with CDK you need to bootstrap the AWS environment you use. Then you can synth deploy and destroy, if you want to remove the policy


Disclaimer
General
This software is provided "as-is" by LearnAutomatedTesting and Bsure Digital B.V., without any warranty or guarantee of any kind, either expressed or implied, including, without limitation, warranties of merchantability, fitness for a particular purpose, or non-infringement. Use of this software is at the user's own risk.

No Liability
Under no circumstances, including negligence, shall LearnAutomatedTesting, Bsure Digital B.V., or any of their contributors or maintainers be liable for any incidental, special, consequential, or direct/indirect damages (including, but not limited to, procurement of substitute goods or services; loss of use, data, or profits; or business interruption) arising out of or related to the use or inability to use the software, even if they have been advised of the possibility of such damages.

No Official Affiliation
While this software is developed by LearnAutomatedTesting and Bsure Digital B.V., it is an independent project and is not officially affiliated with any other company, organization, or institution unless explicitly stated.

Contributions
Contributors to this project, including LearnAutomatedTesting and Bsure Digital B.V., are not responsible for any use of the software by other individuals or entities and are not obligated to provide any support, updates, or fixes.

Open Source License
This software is released under its respective open-source license. All users and contributors, including LearnAutomatedTesting and Bsure Digital B.V., must adhere to the terms and conditions of that license.

