import aws_cdk as core
import aws_cdk.assertions as assertions

from policy_stack.policy_stack_stack import PolicyStackStack

# example tests. To run these tests, uncomment this file along with the example
# resource in policy_stack/policy_stack_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = PolicyStackStack(app, "policy-stack")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
