from aws_cdk import (
    # Duration,
    Stack,
    aws_iam as iam
    # aws_sqs as sqs,
)
from constructs import Construct

class PolicyStackStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

     # Define the IAM Role (if needed)
        role = iam.Role(self, "My_Artillery_Role",
            assumed_by=iam.ServicePrincipal("ecs-tasks.amazonaws.com")
        )

        # Attach policy statements to the role
        statements = [
            iam.PolicyStatement(
                sid="CreateOrGetECSRole",
                actions=["iam:CreateRole", "iam:GetRole"],
                resources=["arn:aws:iam::123456789000:role/artilleryio-ecs-worker-role"],
                effect=iam.Effect.ALLOW
            ),
            iam.PolicyStatement(
                sid="CreateECSPolicy",
                actions=["iam:CreatePolicy", "iam:AttachRolePolicy"],
                resources=["arn:aws:iam::123456789000:policy/ecs-worker-policy"],
                effect=iam.Effect.ALLOW
            ),
            # ... [Add other policy statements similarly]
            iam.PolicyStatement(
                actions=["ec2:DescribeRouteTables", "ec2:DescribeVpcs", "ec2:DescribeSubnets"],
                resources=["*"],
                effect=iam.Effect.ALLOW
            )
        ]

        for statement in statements:
            role.add_to_policy(statement)
        # The code that defines your stack goes here

        # example resource
        # queue = sqs.Queue(
        #     self, "PolicyStackQueue",
        #     visibility_timeout=Duration.seconds(300),
        # )
